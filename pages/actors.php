<?php session_start(); ?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Cinefa, la référence pour vos films préférés">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
        <link href="../script/mdb/css/bootstrap.min.css" rel="stylesheet">
        <link href="../script/mdb/css/mdb.min.css" rel="stylesheet">
        <link href="../script/mdb/css/style.css" rel="stylesheet">
        <link rel="shortcut icon" href="../img/movies.ico" type="image/x-icon">
        <title>Acteurs | Cinefa</title>
        <?php require_once '../connect/config.php'; include '../connect/connexion.php'; include '../connect/function.php'; ?>
    </head>
    <body>

        <?php 
            if (isset($_SESSION['pseudo']) && isset($_SESSION['password']) OR isset($_COOKIE['connect_cinefa'])) 
            {
                $im_connect = 1;
                echo '<p><a href="./logout.php"><i class="fas fa-sign-out-alt"></i>Se déconnecter</a></p>';
            }
            else
            {
                $im_connect = 0;
            } 
        ?>

        <nav class="mb-5 navbar fixed-top navbar-expand-lg navbar-dark info-color">

            <a class="navbar-brand" href="../index.php">Cinefa</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
                aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse " id="navbarSupportedContent-4">
                <ul class="navbar-nav ml-auto ">
                    <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link " href="./movies.php">
                        <i class="fas fa-film"></i> Films
                        <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link" href="./directors.php">
                        <i class="fas fa-video"></i>Réalisateurs</a>
                    </li>
                    <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link" href="./actors.php">
                        <i class="fas fa-star"></i>Acteurs</a>
                    </li>
                    <li class="nav-item dropdown pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fas fa-user"></i><?php if($im_connect) {echo $_COOKIE['connect_cinefa'];} ?></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                            <a class="dropdown-item" href="../profil.php">Mes favoris</a>
                            <?php 
                                if($im_connect)
                                { 
                                    echo '<a class="dropdown-item" href="../logout.php">Se déconnecter</a>';
                                } 
                                else
                                {
                                    echo '<a class="dropdown-item" href="../index.php">Se connecter ou s\'inscrire</a>';
                                }
                            ?>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <nav class="navbar navbar-nav bg-light mb-3 mt-5 mt-5">
            <a class="navbar-brand">Acteurs</a>
            <form action="" method="get" class="form-inline">
                <input name="actors_search" class="form-control mr-sm-2" type="search" placeholder="Recherche" aria-label="Search">
                <button name="search" class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher un acteur</button>
            </form>
        </nav>

        <?php

            if ($im_connect)
            {
                echo '<p class="offset-md-11"><a href="../logout.php"><i class="fas fa-sign-out-alt"></i>Se déconnecter</a></p>';
            }
            else
            {
                echo "<p>Vous n'êtes pas connecté !</p>";
            }

            $verif = "SELECT *, CONCAT(first_name, ' ',last_name) AS name 
            FROM actors";

            if (isset($_GET['search'])) 
            { 
                $actors_search = protect($_GET['actors_search']);

                $search_actor = "SELECT first_name, last_name, CONCAT(first_name, ' ',last_name) AS name, id_actor, photo
                FROM actors
                WHERE CONCAT(first_name, ' ',last_name) LIKE '%$actors_search%' ";

                $search_actor_query = mysqli_query($db_connexion, $search_actor);

                $result_query = mysqli_query($db_connexion, $verif);

                if ($db_select) 
                {
                    $search_actor_response = '';

                    echo '<div class="container-fluid"><div class="row">';

                    while ($search_response = mysqli_fetch_assoc($search_actor_query))
                    {
                        $search_actor_response .= '<p class="col-sm-6 col-md-6 col-lg-4"><a href="./fiche.php?id='.$search_response['id_actor'].'&work=actors&bdd=id_actor"> <img class="img-fluid img-thumbnail" width="50%" title="'. $search_response['name']. '" src="../img/actors/'. $search_response['photo'] .'"></a> <p>';
                    }
                    echo $search_actor_response;
                    echo '</div></div>';

                    
                }

            }
            
            else
            {
                $result_query = mysqli_query($db_connexion, $verif);

                if ($db_select) 
                {
                    echo '<div class="container-fluid"><div class="row">';

                    while ($response = mysqli_fetch_assoc($result_query))
                    {
                        echo '<p class="col-sm-6 col-md-6 col-lg-6"><a href="./fiche.php?id='.$response['id_actor'].'&work=actors&bdd=id_actor"> <img class="img-fluid img-thumbnail" width="50%" title="'. $response['name']. '" src="../img/actors/'. $response['photo'] .'"></a> <p>';                
                    }
                    echo '</div></div>';
                }
            }
            mysqli_close($db_connexion);
        ?>

        <script type="text/javascript" src="../script/mdb/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="../script/mdb/js/popper.min.js"></script>
        <script type="text/javascript" src="../script/mdb/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../script/mdb/js/mdb.js"></script>   
    </body>
</html>