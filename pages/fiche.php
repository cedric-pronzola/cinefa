<?php session_start(); ?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Cinefa, la référence pour vos films préférés">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
        <link href="../script/mdb/css/bootstrap.min.css" rel="stylesheet">
        <link href="../script/mdb/css/mdb.min.css" rel="stylesheet">
        <link href="../script/mdb/css/style.css" rel="stylesheet">
        <link rel="shortcut icon" href="../img/movies.ico" type="image/x-icon">
        <title>Fiches | Cinefa</title>
        <?php require_once '../connect/config.php'; include '../connect/connexion.php'; ?>
    </head>
    <body class="container-fluid">

        <?php
            if (isset($_SESSION['pseudo']) && isset($_SESSION['password']) OR isset($_COOKIE['connect_cinefa'])) 
            {
                $im_connect = 1;
                echo '<p><a href="../logout.php"><i class="fas fa-sign-out-alt"></i> Se déconnecter</a></p>';
            }
            else
            {
                $im_connect = 0;
            }
        ?>

        <nav class="mb-1 navbar sticky-top navbar-expand-lg navbar-dark info-color">

            <a class="navbar-brand" href="../index.php">Cinefa</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
                aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse " id="navbarSupportedContent-4">
                <ul class="navbar-nav ml-auto ">
                    <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link " href="./movies.php">
                        <i class="fas fa-film"></i> Films
                        <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link" href="./directors.php">
                        <i class="fas fa-video"></i>Réalisateurs</a>
                    </li>
                    <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link" href="./actors.php">
                        <i class="fas fa-star"></i>Acteurs</a>
                    </li>
                    <li class="nav-item dropdown pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fas fa-user"></i><?php if($im_connect) {echo $_COOKIE['connect_cinefa'];} ?></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                            <a class="dropdown-item" href="../profil.php">Mes favoris</a>
                            <?php 
                                if($im_connect)
                                { 
                                    echo '<a class="dropdown-item" href="../logout.php">Se déconnecter</a>';
                                } 
                                else
                                {
                                    echo '<a class="dropdown-item" href="../index.php">Se connecter ou s\'inscrire</a>';
                                }
                            ?>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

        <?php

            if (isset($_SESSION['pseudo']) && isset($_SESSION['password']) OR isset($_COOKIE['connect_cinefa']))
            {
                echo "<p></p>";
            }
            else
            {
                echo "<p>Vous n'êtes pas connecté !</p>";
            }

            $id = $_GET['id'];
            $work = $_GET['work'];
            $bdd = $_GET['bdd'];
            $verif = "SELECT *, CONCAT(first_name, ' ',last_name) AS name, DATE_FORMAT(birth_date, '%d-%m-%Y') born, 
            TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) AS age 
            FROM $work 
            WHERE $bdd = '$id'";

            $director_last_movies = "SELECT movies.id_movie, movies.title, DATE_FORMAT(movies.release_date, '%d-%m-%Y') date
            FROM movies
            INNER JOIN directors ON directors.id_director = movies.id_director
            WHERE directors.id_director = '$id' 
            ORDER BY movies.release_date 
            DESC LIMIT 3";

            $actor_last_movies = "SELECT movies.id_movie, movies.title, DATE_FORMAT(movies.release_date, '%d-%m-%Y') date
            FROM movies
            INNER JOIN plays_in ON plays_in.id_movie = movies.id_movie
            INNER JOIN actors ON actors.id_actor = plays_in.id_actor
            WHERE actors.id_actor = '$id' ORDER BY movies.release_date DESC LIMIT 3";

            $result_query = mysqli_query($db_connexion, $verif);
            $director_query = mysqli_query($db_connexion, $director_last_movies);
            $actor_query = mysqli_query($db_connexion, $actor_last_movies);

            if ($db_select) 
            {
                while ($response = mysqli_fetch_assoc($result_query))
                {
                    echo '<p> Prénom : ' . $response['first_name'] . '<p>' . 
                    '<p> Nom : ' . $response['last_name'] . '<p>' . 
                    '<p> Genre : ' . $response['gender'] . '<p>' . 
                    '<p> Date de naissance : ' . $response['born'] . ' (' .$response['age'] . ' ans)<p>' . 
                    '<p> Nationalité : ' . $response['nationality'] . '<p>' .
                    '<p> <a href="#" id="pop">
                            <img class="img-fluid img-thumbnail" id="imageresource" src="../img/'.$work.'/'. $response['photo'] .'" style="width: 200px;">
                        </a>
                        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fermer</span></button>
                                <h4 class="modal-title" id="myModalLabel">'. $response['name'] . '</h4>
                            </div>
                            <div class="modal-body">
                                <img src="" id="imagepreview" style="width: 100%;" >
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            </div>
                            </div>
                        </div>
                        </div> 
                    <p>';

                    $movies_dir = '';
                    $movies_act = '';

                    switch ($work) 
                    {
                        case 'directors':
                            while ($response_director = mysqli_fetch_assoc($director_query))
                            {
                                $movies_dir .= '<a href="./fiche_movies.php?id='. $response_director['id_movie']. '">' . $response_director['title'] . '</a>, ';
                            }
                            echo '<p> Ses trois dernières productions : ' . trim($movies_dir, ', ') ;
                            break;
        
                        case 'actors':
                            while ($response_actor = mysqli_fetch_assoc($actor_query))
                            {
                                $movies_act .= '<a href="./fiche_movies.php?id='. $response_actor['id_movie']. '">' . $response_actor['title'] . '</a>, ';
                            }
                            echo '<p> Ses trois dernières apparitions : ' . trim($movies_act, ', ') ;
                            break;
        
                        default:
        
                            break;
                    }

                    echo '<p>  
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalScrollable">
                                Biographie
                            </button>
                            <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalScrollableTitle">'. $response['name'] . '</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            '. $response['infos'] . '
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        <p>' ;
                }
            }
            mysqli_close($db_connexion);
        ?>

    
        <script type="text/javascript" src="../script/mdb/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="../script/mdb/js/popper.min.js"></script>
        <script type="text/javascript" src="../script/mdb/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../script/mdb/js/mdb.js"></script>
        <script>
            $("#pop").on("click", function() {
            $('#imagepreview').attr('src', $('#imageresource').attr('src'));
            $('#imagemodal').modal('show'); 
            });
        </script> 
    </body>
</html>