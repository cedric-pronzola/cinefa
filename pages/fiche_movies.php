<?php session_start(); ?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Cinefa, la référence pour vos films préférés">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
        <link href="../script/mdb/css/bootstrap.min.css" rel="stylesheet">
        <link href="../script/mdb/css/mdb.min.css" rel="stylesheet">
        <link href="../script/mdb/css/style.css" rel="stylesheet">
        <link rel="shortcut icon" href="../img/movies.ico" type="image/x-icon">
        <title>Fiche Films | Cinefa</title>
        <?php require_once '../connect/config.php'; include '../connect/connexion.php'; ?>
    </head>
    <?php

        if (isset($_SESSION['pseudo']) && isset($_SESSION['password']) OR isset($_COOKIE['connect_cinefa'])) 
        {
            $im_connect = 1;
            echo '<p><a href="../logout.php"><i class="fas fa-sign-out-alt"></i> Se déconnecter</a></p>';
        }
        else
        {
            $im_connect = 0;
        }
    ?>
    <body class="container-fluid">

        <nav class="mb-1 navbar sticky-top navbar-expand-lg navbar-dark info-color">

            <a class="navbar-brand" href="../index.php">Cinefa</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
                aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse " id="navbarSupportedContent-4">
                <ul class="navbar-nav ml-auto ">
                    <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link " href="./movies.php">
                        <i class="fas fa-film"></i> Films
                        <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link" href="./directors.php">
                        <i class="fas fa-video"></i>Réalisateurs</a>
                    </li>
                    <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link" href="./actors.php">
                        <i class="fas fa-star"></i>Acteurs</a>
                    </li>
                    <li class="nav-item dropdown pl-5 pr-5 flex-fill bd-highlight">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fas fa-user"></i><?php if($im_connect) {echo $_COOKIE['connect_cinefa'];} ?></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                            <a class="dropdown-item" href="../profil.php">Mes favoris</a>
                            <?php 
                            if($im_connect)
                            { 
                                echo '<a class="dropdown-item" href="../logout.php">Se déconnecter</a>';
                            } 
                            else
                            {
                                echo '<a class="dropdown-item" href="../index.php">Se connecter ou s\'inscrire</a>';
                            }
                            ?>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

        <?php

            if (isset($_SESSION['pseudo']) && isset($_SESSION['password']) OR isset($_COOKIE['connect_cinefa']))
            {

                $autorize_pseudo = $_COOKIE['connect_cinefa'];

                $noter_un_film = 1;
                $add_to_list = 1;
            }
            else
            {
                echo '<p>Vous n\'êtes pas connecté ! <a href="../index.php">Se connecter</a> ou <a href="../index.php">s\'enregistrer</a></p>';
                $noter_un_film = 0;
                $add_to_list = 0;
                $autorize_pseudo ='';
            }

            $id = $_GET['id'];
            $verif = "SELECT *, DATE_FORMAT(release_date, '%d-%m-%Y') date 
            FROM movies 
            WHERE id_movie = '$id'";

            $genre = "SELECT genre.name
            FROM genre
            INNER JOIN genre_movies ON genre_movies.id_genre = genre.id_genre
            INNER JOIN movies ON movies.id_movie = genre_movies.id_movie
            WHERE movies.id_movie = '$id'";

            $director_name = "SELECT directors.id_director, directors.first_name, directors.last_name, CONCAT (directors.first_name, ' ', directors.last_name) AS director_name_concat
            FROM directors
            INNER JOIN movies ON movies.id_director = directors.id_director
            WHERE movies.id_movie = '$id'";

            $actor_name = "SELECT actors.id_actor, actors.first_name, actors.last_name, CONCAT (actors.first_name, ' ', actors.last_name) AS actor_name_concat
            FROM actors
            INNER JOIN plays_in ON plays_in.id_actor = actors.id_actor
            INNER JOIN movies ON plays_in.id_movie = movies.id_movie
            WHERE movies.id_movie = '$id'";

            $movie_note = "SELECT movies.id_movie, movies.title, ROUND(AVG(movies_notes.note),1) AS moyenne
            FROM movies
            INNER JOIN movies_notes ON movies_notes.id_movie = movies.id_movie
            WHERE movies.id_movie = '$id'";

            $what_id = "SELECT users.id_user
            FROM users
            WHERE users.pseudo = '$autorize_pseudo'";

            $which_vote = "SELECT movies_notes.id_movie, movies_notes.id_user
            FROM movies_notes
            INNER JOIN users ON users.id_user = movies_notes.id_user
            WHERE users.pseudo = '$autorize_pseudo' AND movies_notes.id_movie = '$id'";

            $result_query = mysqli_query($db_connexion, $verif);
            $query_genre = mysqli_query($db_connexion, $genre);
            $query_director = mysqli_query($db_connexion, $director_name);
            $query_actor = mysqli_query($db_connexion, $actor_name);
            $query_note = mysqli_query($db_connexion, $movie_note);
            $query_what_id = mysqli_query($db_connexion, $what_id);
            $query_which_vote = mysqli_query($db_connexion, $which_vote);

            if ($db_select) 
            {
                $movie_genre = '';
                $movie_actor='';

                $response = mysqli_fetch_assoc($result_query);
                $response_director = mysqli_fetch_assoc($query_director);
                $response_note = mysqli_fetch_assoc($query_note);
                $response_what_id = mysqli_fetch_assoc($query_what_id);
                $response_which_vote = mysqli_fetch_assoc($query_which_vote);

                $user_id = $response_what_id['id_user'];

                $movie_id_movie_vote = $response_which_vote['id_movie'];
                $user_id_movie_vote = $response_which_vote['id_user'];


                while ($response_genre = mysqli_fetch_assoc($query_genre))
                {
                    $movie_genre .= $response_genre['name'] . ', ';
                }

                while ($response_actor = mysqli_fetch_assoc($query_actor))
                {
                    $movie_actor .= '<a href="./fiche.php?id='. $response_actor['id_actor'].'&work=actors&bdd=id_actor">'. $response_actor['actor_name_concat'] . '</a>' . ', ' ;
                }

                $movie_director = '<a href="./fiche.php?id=' . $response_director['id_director'] . '&work=directors&bdd=id_director">'. $response_director['director_name_concat'] . '</a>';

                $youtube = "https://www.youtube.com/embed/" . $response['trailer'];
                $integrate = '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalYT">Bande Annonce</button>
                <div class="modal fade" id="modalYT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body mb-0 p-0">
                                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                    <iframe class="embed-responsive-item" src="'. $youtube.'" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-center flex-column flex-md-row">
                                <button type="button" class="btn btn-outline-danger btn-rounded btn-md ml-4" data-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>';

                $img_integrate = '<a href="#" id="pop">
                <img class="img-fluid img-thumbnail" id="imageresource" src="../img/movies/'. $response['poster'] .'" style="width: 200px;"></a>
            
                <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fermer</span></button>
                        <h4 class="modal-title" id="myModalLabel">'. $response['title'] . '</h4>
                    </div>
                    <div class="modal-body">
                        <img src="" id="imagepreview" style="width: 100%;" >
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                    </div>
                </div>
                </div>';

                echo '<p>' . $response['title'] . '<p>' . 
                '<p> Date de sortie : ' . $response['date'] . '<p>' .
                '<p> Genre : ' . trim($movie_genre, ' ,') . ' <p>' . 
                '<p> Réalisateur : ' . $movie_director . '<p>' .
                '<p> Casting : ' . trim($movie_actor, ' ,') . '<p>' . 
                '<p> Note du film : ' . $response_note['moyenne'] ;

                    // Voter pour un film
            
                if ($noter_un_film == 1 AND ($movie_id_movie_vote != $id) AND ($user_id_movie_vote != $user_id))
                {
                    echo '<form class="form-inline" action="" method="post">
                    <label class="mr-3" for="vote_film">Votez pour ce film : </label>
                        <select name="vote" class="form-control col-md-1" id="vote_film">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        <button type="submit" name="submit" class="btn btn-primary ml-3 mb-2">Votez</button>
                    </form>';
                }
                elseif ($noter_un_film == 1 AND ($movie_id_movie_vote == $id) AND ($user_id_movie_vote == $user_id))
                {
                
                }
                elseif (!$noter_un_film) 
                {
                    echo '<p><a href="../index.php">Connectez-vous</a> pour évaluer ce film</p>';
                }
                else 
                {
                    echo "oups il y a un problème";
                }

                    // Ajouter à une liste

                if ($add_to_list) 
                {

                    $whats_my_fav = "SELECT categories.*, DATE_FORMAT(categories.creation_date, '%d-%m-%Y') creation, categories.id_user
                    FROM categories
                    WHERE categories.id_user = '$user_id'";

                    $whats_my_fav_query = mysqli_query($db_connexion, $whats_my_fav);

                    if ($db_select) 
                    {
                        $list_my_fav = '';

                        echo '<p class="mt-3"><button type="button" data-toggle="modal" data-target="#exampleModalCenter" 
                        class="btn btn-warning" name="show_list">Ajouter à une liste</button></p>';

                        echo '<form action="" method="post">
                                <div class="modal fade col-m8" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">Ajouter ce film à la liste</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">';

                        while ($my_fav = mysqli_fetch_assoc($whats_my_fav_query)) 
                        {
                            $list_my_fav = $my_fav['category_title'];
                            $list_my_fav_id = $my_fav['id_category'];

                            echo '<div class="form-check"><input class="form-check-input" id="'. $list_my_fav_id .'" type="radio" value="'. $list_my_fav_id .'" name="the_list_name">
                                    <label class="form-check-label" for="'. $list_my_fav_id .'">'. $list_my_fav . '</div> ';
                        }
                            echo '</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" name="ajouter_liste" class="btn btn-primary">Ajouter à la liste</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>';
                    }

                    if (isset($_POST['ajouter_liste'])) 
                    {

                        if (isset($_POST['the_list_name'])) 
                        {
                            $add_to_my_list = $_POST['the_list_name'];

                            $insert_into_cat = "INSERT INTO category_content(id_movie, id_category)
                            VALUES ('$id', '$add_to_my_list')";

                            $insert_into_cat_query = mysqli_query($db_connexion, $insert_into_cat);

                            if ($insert_into_cat_query) 
                            {
                                echo "<p>Votre film a été rajouté à la liste</p>";
                            }
                            else
                            {
                                echo "<p>Ce film est déjà dans cette liste</p>";
                            }
                        }
                        else
                        {
                            echo "<p>Veuillez sélectionner ou créer une liste.</p>";
                        }
                    }
                }

                if (isset($_POST['submit'])) 
                {
                    $vote = $_POST['vote'];

                    $put_a_note = "INSERT INTO movies_notes(id_movie, id_user, note)
                    VALUES ('$id', '$user_id', '$vote')";

                    $query_put_a_note = mysqli_query($db_connexion, $put_a_note);

                    if ($query_put_a_note) 
                    {
                        echo "<p>Merci d'avoir voté</p>";
                    }
                    else 
                    {
                        echo '<p>Vous avez déjà voté</p>';
                    }
                }

                echo '<p> Synopsis : ' . $response['synopsis'] . '<p>' . 
                '<p>'. $img_integrate . ' <p>' . 
                '<p>' . $integrate . '<p>';
            }
            mysqli_close($db_connexion);
        ?>

        <script type="text/javascript" src="../script/mdb/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="../script/mdb/js/popper.min.js"></script>
        <script type="text/javascript" src="../script/mdb/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../script/mdb/js/mdb.js"></script> 
        <script>
            $("#pop").on("click", function() {
            $('#imagepreview').attr('src', $('#imageresource').attr('src'));
            $('#imagemodal').modal('show'); 
            });
        </script>
    </body>
</html>

