<?php require_once './connect/config.php'; include './connect/connexion.php'; include './connect/function.php'?>
    
    <?php

        if (isset($_POST['submit'])) 
        {

            $pseudo = protect($_POST['pseudo']);  // Ligne 4 dans la page "./connect/function.php"
            $password = protect($_POST['password']);
            $mail = protect($_POST['mail']);
            $genre = ($_POST['genre']);
            $age = ($_POST['age']);

            $secure = secure($password);  // Ligne 12 dans la page "./connect/function.php"
         
            if ($db_select) 
            {
                $verif_user = "SELECT pseudo
                FROM users
                WHERE pseudo = '$pseudo'";

                $verif_mail = "SELECT mail
                FROM users
                WHERE mail = '$mail'";

                $verif_user_query = mysqli_query($db_connexion, $verif_user);
                $verif_mail_query = mysqli_query($db_connexion, $verif_mail);

                $response_verif_user = mysqli_fetch_assoc($verif_user_query);
                $response_verif_mail = mysqli_fetch_assoc($verif_mail_query);

                if ($response_verif_user['pseudo'] == $pseudo) 
                {
                    echo "<h1>Ce pseudo est déjà utilisé. Choisissez un autre</h1>";
                    echo '<h2>Rendez-vous <a href="./index.php">à l\'accueil</a></h2>';
                }
                elseif ($response_verif_mail['mail'] == $mail)
                {
                    echo "<h1>Cette adresse mail est déjà utilisée. Choisissez une autre.</h1>";
                    echo '<h2>Rendez-vous <a href="./index.php">à l\'accueil</a></h2>';
                }
                else 
                {
                    $add_bd = "INSERT INTO users(pseudo, password, mail, genre, age, register_date)
                    VALUES ('$pseudo', '$secure', '$mail', '$genre', '$age', CURDATE())";
                    $result_query = mysqli_query($db_connexion, $add_bd);
    
                    if ($result_query)
                    {
                        echo '<h1>Vous êtes enregistré.  <a href="./index.php">Veuillez vous connecter.</h1>';
                    }
                    else
                    {
                        echo "<p>Une erreur s'est produite vous n'êtes pas enregistré.</p>";
                    }
                }
            }
        
        }
    ?>
        <script src="./script/bootstrap/js/jquery-3.3.1.slim.min.js"></script>
        <script src="./script/bootstrap/js/popper.min.js"></script>
        <script src="./script/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>