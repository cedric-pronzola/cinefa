<?php session_start(); ?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Cinefa, la référence pour vos films préférés">
        <title>Cinefa | by Cédric FAMIBELLE</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
        <link href="./script/mdb/css/bootstrap.min.css" rel="stylesheet">
        <link href="./script/mdb/css/mdb.min.css" rel="stylesheet">
        <link href="./script/mdb/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="./script/style.css">
        <link rel="shortcut icon" href="./img/movies.ico" type="image/x-icon">
        <?php require_once './connect/config.php'; include './connect/connexion.php'; include './login.php'; ?>
    </head>
    <body class="pt-1 container-fluid">

    <?php

    if (isset($_SESSION['pseudo']) && isset($_SESSION['password']) OR isset($_COOKIE['connect_cinefa'])) 
    {
        $im_connect = 1;
    }
    else
    {
      $im_connect = 0;
      echo '<form class="pt-1" action="./login.php" method="post">
                    <div class="modal fade" id="modalLRForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog cascading-modal" role="document">
                    <div class="modal-content">
                    <div class="modal-c-tabs">
                        <ul class="nav nav-tabs md-tabs tabs-2 light darken-3" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab"><i class="fas fa-user mr-1"></i>
                              Se connecter</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link light" data-toggle="tab" href="#panel8" role="tab"><i class="fas fa-user-plus mr-1"></i>
                              S\'inscrire</a>
                          </li>
                        </ul>
                        <div class="tab-content">
                          <div class="tab-pane fade in show active" id="panel7" role="tabpanel">
                            <div class="modal-body mb-1">
                              <div class="md-form form-sm mb-5">
                                <i class="fas fa-user prefix grey-text"></i>
                                <input type="text" id="modalLRInput10" class="form-control form-control-sm validate" name="pseudo" minlegth="3" maxlength="15" pattern="^[a-z-0-9_]{3,15}$" required>
                                <label t" for="modalLRInput10">Pseudo</label>
                              </div>
                
                              <div class="md-form form-sm mb-4">
                                <i class="fas fa-lock prefix"></i>
                                <input type="password" name="password" id="modalLRInput11" minlegth="3" required class="form-control form-control-sm validate">
                                <label ht" for="modalLRInput11">Mot de passe</label>
                              </div>
                              <div class="text-center mt-2">
                                <button type="submit" name="submit2" formaction="./login.php" class="btn btn-info">Se connecter<i class="fas fa-sign-in ml-1"></i></button>
                              </div>
                            </div>
                            </form>
                            <div class="modal-footer">

                              <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Fermer</button>
                            </div>
                
                          </div>
                          <div class="tab-pane fade" id="panel8" role="tabpanel">
                
                            <form action="./register.php" method="post">
                            <div class="modal-body">
                              <div class="md-form form-sm mb-2">
                                <i class="fas fa-user prefix grey-text"></i>
                                <input type="text" id="modalLRInput12" class="form-control form-control-sm validate" name="pseudo" minlegth="3" maxlength="15" pattern="^[a-z-0-9_]{3,15}$" required>
                                <label for="modalLRInput12">Pseudo</label>
                                <small id="passwordHelpBlock" class="form-text text-muted">
                                    De 3 à 16 caractères. Pas d\'espace ni de majuscule. Le seul caractère spécial accepté est l\'underscore "_".
                                </small>
                              </div>
                                  <div class="md-form form-sm mb-3">
                                <i class="fas fa-envelope prefix grey-text"></i>
                                <input type="email" id="modalLRInput13" name="mail" class="form-control form-control-sm validate" pattern="[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([_\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})" size="25" required>
                                <label for="modalLRInput13">Mail</label>
                              </div>
                
                              <div class="md-form form-sm mb-3">
                                <i class="fas fa-lock prefix"></i>
                                <input type="password" name="password" id="modalLRInput14" class="form-control form-control-sm validate" minlegth="3" required>
                                <label for="modalLRInput14">Mot de passe</label>
                              </div>

                              <div class="md-form form-sm mb-5">
                                <select class="form-control" name="genre" id="selectGenre">
                                    <option value="homme">Homme</option>
                                    <option value="femme">Femme</option>
                                </select>
                              </div>

                              <div class="md-form form-sm mb-5">
                              <select class="form-control" name="age" id="selectAge">
                                    <option value="10">10 ans</option>
                                    <option value="11">11 ans</option>
                                    <option value="12">12 ans</option>
                                    <option value="13">13 ans</option>
                                    <option value="14">14 ans</option>
                                    <option value="15">15 ans</option>
                                    <option value="16">16 ans</option>
                                    <option value="17">17 ans</option>
                                    <option value="18">18 ans</option>
                                    <option value="19">19 ans</option>
                                    <option value="20">20 ans</option>
                                    <option value="21">21 ans</option>
                                    <option value="22">22 ans</option>
                                    <option value="23">23 ans</option>
                                    <option value="24">24 ans</option>
                                    <option value="25">25 ans</option>
                                    <option value="26">26 ans</option>
                                    <option value="27">27 ans</option>
                                    <option value="28">28 ans</option>
                                    <option value="29">29 ans</option>
                                    <option value="30">30 ans</option>
                                    <option value="31">31 ans</option>
                                    <option value="32">32 ans</option>
                                    <option value="33">33 ans</option>
                                    <option value="34">34 ans</option>
                                    <option value="35">35 ans</option>
                                    <option value="36">36 ans</option>
                                    <option value="37">37 ans</option>
                                    <option value="38">38 ans</option>
                                    <option value="39">39 ans</option>
                                    <option value="40">40 ans</option>
                                    <option value="41">41 ans</option>
                                    <option value="42">42 ans</option>
                                    <option value="43">43 ans</option>
                                    <option value="44">44 ans</option>
                                    <option value="45">45 ans</option>
                                    <option value="46">46 ans</option>
                                    <option value="47">47 ans</option>
                                    <option value="48">48 ans</option>
                                    <option value="49">49 ans</option>
                                    <option value="50">50 ans</option>
                                    <option value="51">51 ans</option>
                                    <option value="52">52 ans</option>
                                    <option value="53">53 ans</option>
                                    <option value="54">54 ans</option>
                                    <option value="55">55 ans</option>
                                    <option value="56">56 ans</option>
                                    <option value="57">57 ans</option>
                                    <option value="58">58 ans</option>
                                    <option value="59">59 ans</option>
                                    <option value="60">60 ans</option>
                                    <option value="61">61 ans</option>
                                    <option value="62">62 ans</option>
                                    <option value="63">63 ans</option>
                                    <option value="64">64 ans</option>
                                    <option value="65">65 ans</option>
                                    <option value="66">66 ans</option>
                                    <option value="67">67 ans</option>
                                    <option value="68">68 ans</option>
                                    <option value="69">69 ans</option>
                                    <option value="70">70 ans</option>
                                    <option value="71">71 ans</option>
                                    <option value="72">72 ans</option>
                                    <option value="73">73 ans</option>
                                    <option value="74">74 ans</option>
                                    <option value="75">75 ans</option>
                                    <option value="76">76 ans</option>
                                    <option value="77">77 ans</option>
                                    <option value="78">78 ans</option>
                                    <option value="79">79 ans</option>
                                    <option value="80">80 ans</option>
                                    <option value="81">81 ans</option>
                                    <option value="82">82 ans</option>
                                    <option value="83">83 ans</option>
                                    <option value="84">84 ans</option>
                                    <option value="85">85 ans</option>
                                    <option value="86">86 ans</option>
                                    <option value="87">87 ans</option>
                                    <option value="88">88 ans</option>
                                    <option value="89">89 ans</option>
                                    <option value="90">90 ans</option>
                                    <option value="91">91 ans</option>
                                    <option value="92">92 ans</option>
                                    <option value="93">93 ans</option>
                                    <option value="94">94 ans</option>
                                    <option value="95">95 ans</option>
                                    <option value="96">96 ans</option>
                                    <option value="97">97 ans</option>
                                    <option value="98">98 ans</option>
                                    <option value="99">99 ans</option>
                                </select>
                              </div>
                
                              <div class="text-center form-sm mt-2">
                                <button type="submit" name="submit" class="btn btn-info">S\'enregistrer<i class="fas fa-sign-in ml-1"></i></button>
                              </div>
                
                            </div>
                            <div class="modal-footer">

                              <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    </div>
                    <div class="text-center">
                    <a href="" class="btn btn-default btn-rounded mt-1 my-1" data-toggle="modal" data-target="#modalLRForm">Se connecter ou s\'inscrire</a>
                    </div>
              </form>';
    }


    ?>

        <nav class="mt-1 mb-1 navbar sticky-top navbar-expand-lg navbar-dark info-color">

          <a class="navbar-brand" href="./index.php">Cinefa</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
              aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse " id="navbarSupportedContent-4">
              <ul class="navbar-nav ml-auto ">
                <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                    <a class="nav-link " href="./pages/movies.php">
                    <i class="fas fa-film"></i> Films
                    <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                    <a class="nav-link" href="./pages/directors.php">
                    <i class="fas fa-video"></i>Réalisateurs</a>
                </li>
                <li class="nav-item pl-5 pr-5 flex-fill bd-highlight">
                    <a class="nav-link" href="./pages/actors.php">
                    <i class="fas fa-star"></i>Acteurs</a>
                </li>
                <li class="nav-item dropdown pl-5 pr-5 flex-fill bd-highlight">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <i class="fas fa-user"></i><?php if($im_connect) {echo $_COOKIE['connect_cinefa'];} ?></a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                        <a class="dropdown-item" href="./profil.php">Mes favoris</a>
                        <?php if($im_connect)
                        { 
                          echo '<a class="dropdown-item" href="./logout.php">Se déconnecter</a>';
                        } 
                        else
                        {
                            echo '<a class="dropdown-item" href="./index.php">Se connecter ou s\'inscrire</a>';
                        }
                            
                        ?>
                    </div>
                </li>
              </ul>
          </div>

        </nav>
        
        <div class="bg"></div>
        
        <footer class="page-footer font-small cyan darken-3">
            <div class="container">
              <div class="row">
                <div class="col-md-12 py-5">
                  <div class="mb-5 flex-center">
                    <!-- Gitlab -->
                    <a title="GitLab" href="https://gitlab.com/cedric_famibelle/cinefa" target="_blank">
                      <i class="fab fa-gitlab fa-lg white-text mr-md-5 mr-3 ml-5 fa-2x"> </i>
                    </a>
                    <!-- GitHub-->
                    <a title="GitHub" href="https://github.com/cedric-famibelle" target="_blank">
                      <i class="fab fa-github fa-lg white-text mr-md-5 mr-3 ml-5 fa-2x"> </i>
                    </a>
                    <!--Linkedin -->
                    <a title="LinkedIn" href="https://www.linkedin.com/in/cedric-famibelle-pronzola/" target="_blank">
                      <i class="fab fa-linkedin-in fa-lg white-text mr-md-5 ml-5 mr-3 fa-2x"> </i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="footer-copyright text-center py-3">Cinefa by Cédric FAMIBELLE </a>
            </div>
        </footer>
        <script type="text/javascript" src="./script/mdb/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="./script/mdb/js/popper.min.js"></script>
        <script type="text/javascript" src="./script/mdb/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="./script/mdb/js/mdb.js"></script>
    </body>
    
</html>